# imooc-security

#### 项目介绍
基于Spring Security + Spring Boot实现的规范的、完整的认证授权公用模块

#### 软件架构
软件架构说明

认证逻辑
1. 客户端：
1.1. 浏览器认证：
    1.1.1. 认证方式：
    1.1.1.1. 表单登陆
表单认证流程详解图（源码级）
![输入图片说明](https://images.gitee.com/uploads/images/2018/1013/022112_b9f0c2cd_1518526.jpeg "SpringSecurity表单登陆详解图.jpg")
#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)