package com.imooc.dto;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.Past;

import com.fasterxml.jackson.annotation.JsonView;
import com.imooc.validator.MyConstraint;

/**
 * 
 * @author JZH
 * 
 * @JsonView：针对不认同的场景对返回的对象的属性进行选择性展示 
 * 1. 定义接口（即场景） 
 * 2. 属性getter方法上添加所实适用的场景 
 * 3. 再方法上选择应用的场景
 */
public class User implements Serializable {

	// 简单的信息
	public interface UserSinpleView {
	}

	// 用户列表
	public interface UserListView extends UserSinpleView {
	}

	// 用户详情
	public interface UserInfoView extends UserListView {
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer id;

	private String name;

	@MyConstraint(message = "用户名不存在")
	private String loginName;

//	@NotBlank(message = "密码不能为空")
	private String password;

	private Integer age;
	
	@Past(message = "生日必须是历史事件")
	private Date birthday;

	@JsonView(UserListView.class)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@JsonView(UserSinpleView.class)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@JsonView(UserSinpleView.class)
	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	@JsonView(UserInfoView.class)
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@JsonView(UserSinpleView.class)
	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	@JsonView(UserSinpleView.class)
	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public User() {
		super();
	}

	public User(Integer id, String name, String loginName, String password, Integer age) {
		super();
		this.id = id;
		this.name = name;
		this.loginName = loginName;
		this.password = password;
		this.age = age;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", name=" + name + ", loginName=" + loginName + ", password=" + password + ", age="
				+ age + ", birthday=" + birthday + "]";
	}

	

}
