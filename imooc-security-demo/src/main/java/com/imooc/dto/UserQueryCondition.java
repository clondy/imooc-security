package com.imooc.dto;

import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;

/**
 * 請求參數過多的時候，可以使用該類的方式集中聲明
 * 注意：此時下列查詢條件均不是必須
 * @author JZH
 *
 */
public class UserQueryCondition implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(value = "用户姓名")
	private String name;
	
	@ApiModelProperty(value = "用户年龄")
	private int age;
	
	@ApiModelProperty(value = "用户性别（男 女）")
	private String gender;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "UserQueryCondition [name=" + name + ", age=" + age + ", gender=" + gender + "]";
	}
	
	

}
