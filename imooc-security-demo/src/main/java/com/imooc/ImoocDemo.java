package com.imooc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@SpringBootApplication
public class ImoocDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SpringApplication.run(ImoocDemo.class, args);
	}

}
