package com.imooc.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import com.imooc.service.UserService;

/**
 * 自定义校验器
 * @author JZH
 *
 */
public class AuthenticationValidator implements ConstraintValidator<MyConstraint, String>{

	@Autowired
	private UserService userService;
	
	@Override
	public void initialize(MyConstraint constraintAnnotation) {
	}
	
	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		// TODO Auto-generated method stub
		return userService.hasUserInDb(value);
	}

}
