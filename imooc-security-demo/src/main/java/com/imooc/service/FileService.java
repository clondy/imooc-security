package com.imooc.service;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.imooc.dto.FileInfo;

@Service
public class FileService {

	public FileInfo uploadFile(MultipartFile file) throws IllegalStateException, IOException {
		String folder = "D:\\文件\\测试";
		File localFile = new File(folder, new Date().getTime()+".txt");
		file.transferTo(localFile);//将上传的文件写入指定文件中
		return new FileInfo(localFile.getAbsolutePath());
	}
	
}
