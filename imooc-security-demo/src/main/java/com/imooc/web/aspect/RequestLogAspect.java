package com.imooc.web.aspect;

import java.time.Duration;
import java.time.LocalDateTime;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class RequestLogAspect {

	@Around("execution(* com.imooc.web.controller.UserController.*(..))")
	public Object log(ProceedingJoinPoint pjp) throws Throwable {
		LocalDateTime startTime = LocalDateTime.now();
		System.out.println("【AOP】开始时间："+startTime);
		Object obj = pjp.proceed();
		LocalDateTime endTime = LocalDateTime.now();
		System.out.println("【AOP】结束时间："+endTime);
		Duration duration = Duration.between(startTime, endTime);
		Long d = duration.toMillis();
		System.out.println("【AOP】耗时："+d);
		Object[] args = pjp.getArgs();
		System.out.println("【AOP】请求参数：");
		for(Object arg : args) {
			System.out.println("   --- " + arg);
		}
		return obj;
	}
	
}
