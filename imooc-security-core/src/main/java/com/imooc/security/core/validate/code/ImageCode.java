package com.imooc.security.core.validate.code;

import java.awt.image.BufferedImage;
import java.time.LocalDateTime;

/**
 * 图型验证码
 * @author JZH
 *
 */
public class ImageCode extends ValidateCode{

	private BufferedImage image;
	
	/**
	 * 
	 * @param image
	 * @param code
	 * @param expireIn 多少秒内过期
	 */
	public ImageCode(BufferedImage image, String code, int expireIn) {
		super(code, expireIn);
		this.image = image;
	}
	
	public ImageCode(BufferedImage image, String code, LocalDateTime localDateTime) {
		super(code, localDateTime);
		this.image = image;
	}

	public BufferedImage getImage() {
		return image;
	}

	public void setImage(BufferedImage image) {
		this.image = image;
	}

	@Override
	public String toString() {
		return "ImageCode [code=" + getCode() + ", localDateTime=" + getLocalDateTime() + "]";
	}
	
}
