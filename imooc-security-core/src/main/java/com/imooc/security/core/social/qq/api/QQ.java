package com.imooc.security.core.social.qq.api;

/**
 * 第三方软件获取用户信息（服务提供商：QQ）
 * @author JZH
 *
 */
public interface QQ {

	public QQUserInfo getQQUserInfo();
	
}
