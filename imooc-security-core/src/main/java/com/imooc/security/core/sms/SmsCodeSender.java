package com.imooc.security.core.sms;

/**
 * 短信验证码发送接口
 * @author JZH
 *
 */
public interface SmsCodeSender {

	void send(String mobile, String code);
	
}
