package com.imooc.security.core.validate.code;

import org.springframework.web.context.request.ServletWebRequest;

/**
 * 抽象的验证码处理器
 * 生成校验码（生成，存储，返回）
 * 校验验证码是否正确
 * @author JZH
 *
 */
public interface ValidateCodeProcessor {
	
	/**
	 * 验证码在session中的KEY前缀
	 */
	String SESSION_KEY_PREFIX = "SESSION_KEY_IMAGECODE_";
	
	/**
	 * 生成验证码模板方法
	 * 生成校验码、放入session、发送校验码
	 * @param request Spring的一个工具类，封装请求和相应，request和response可以都放入进去
	 */
	void createCode(ServletWebRequest request) throws Exception;
	
	/**
	 * 在过滤器中校验验证码是否正确
	 * @param request
	 */
	void validate(ServletWebRequest request);

}
