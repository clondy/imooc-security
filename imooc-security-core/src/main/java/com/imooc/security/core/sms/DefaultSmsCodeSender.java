package com.imooc.security.core.sms;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 默认短信验证码发送实现
 * @author JZH
 *
 */
public class DefaultSmsCodeSender implements SmsCodeSender {

	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Override
	public void send(String mobile, String code) {
		// TODO Auto-generated method stub
		logger.info("【发送短信】向"+mobile+"手机发送验证码："+code);
	}

}
