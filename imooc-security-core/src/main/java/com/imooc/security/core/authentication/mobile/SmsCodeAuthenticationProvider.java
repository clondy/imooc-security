package com.imooc.security.core.authentication.mobile;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * 实际认证逻辑（通过手机号登陆）
 * @author JZH
 *
 */
public class SmsCodeAuthenticationProvider implements AuthenticationProvider{
	
	private UserDetailsService userDetailsService;

	/**
	 * 用户认证逻辑
	 */
	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		// TODO Auto-generated method stub
		SmsCodeAuthenticationToken token = (SmsCodeAuthenticationToken) authentication;
		UserDetails user = userDetailsService.loadUserByUsername((String) token.getPrincipal());
		if(null == user) {
			throw new InternalAuthenticationServiceException("无法获取用户信息");
		}
		//认证成功后，重新封装用户信息，增加用户权限，认证标志置成true
		SmsCodeAuthenticationToken newToken = new SmsCodeAuthenticationToken(user, user.getAuthorities());
		//请求信息也封装进去
		newToken.setDetails(token.getDetails());
		return newToken;
	}

	/**
	 * 只接收SmsCodeAuthenticationToken进行验证逻辑
	 */
	@Override
	public boolean supports(Class<?> authentication) {
		// TODO Auto-generated method stub
		return SmsCodeAuthenticationToken.class.isAssignableFrom(authentication);
	}

	public UserDetailsService getUserDetailsService() {
		return userDetailsService;
	}

	public void setUserDetailsService(UserDetailsService userDetailsService) {
		this.userDetailsService = userDetailsService;
	}

}
