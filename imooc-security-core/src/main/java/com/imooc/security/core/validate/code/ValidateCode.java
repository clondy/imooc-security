package com.imooc.security.core.validate.code;

import java.time.LocalDateTime;

/**
 * 短信验证码（是图形验证码的基类）
 * @author JZH
 *
 */
public class ValidateCode {

	private String code;
	
	private LocalDateTime localDateTime; //过期时间

	/**
	 * 
	 * @param image
	 * @param code
	 * @param expireIn 多少秒内过期
	 */
	public ValidateCode(String code, int expireIn) {
		super();
		this.code = code;
		this.localDateTime = LocalDateTime.now().plusSeconds(expireIn);
	}
	
	public ValidateCode(String code, LocalDateTime localDateTime) {
		super();
		this.code = code;
		this.localDateTime = localDateTime;
	}

	public String getCode() {
		return code;
	}

	@Override
	public String toString() {
		return "ImageCode [code=" + code + ", localDateTime=" + localDateTime + "]";
	}

	public void setCode(String code) {
		this.code = code;
	}

	public LocalDateTime getLocalDateTime() {
		return localDateTime;
	}

	public void setLocalDateTime(LocalDateTime localDateTime) {
		this.localDateTime = localDateTime;
	}
	
	public boolean isExpried() {
		return LocalDateTime.now().isAfter(localDateTime);
	}
	
}
