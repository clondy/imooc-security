package com.imooc.security.core.validate.code.image;

import javax.imageio.ImageIO;

import org.springframework.stereotype.Component;
import org.springframework.web.context.request.ServletWebRequest;

import com.imooc.security.core.validate.code.ImageCode;
import com.imooc.security.core.validate.code.impl.AbstractValidateCodeProcessor;

/**
 * 根据模板模抽象类实现的图形验证码生成发送器
 * @author JZH
 *
 */
@Component("imageValidateCodeProcessor")
public class ImageCodeProcessor extends AbstractValidateCodeProcessor<ImageCode>{

	@Override
	public void send(ServletWebRequest request, ImageCode t)  throws Exception {
		// TODO Auto-generated method stub
		ImageIO.write(t.getImage(), "JPEG", request.getResponse().getOutputStream());
	}

	

}
