package com.imooc.security.core.validate.code;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.imooc.security.core.properties.SecurityProperties;
import com.imooc.security.core.sms.DefaultSmsCodeSender;
import com.imooc.security.core.validate.code.image.ImageCodeGenerator;

/**
 * BeanConfig
 * @author JZH
 * 
 * 注入：
 * 1. 图形验证码生成器（用户可以自己实现，bean名称：imageValidateCodeGenerator）
 * 2. 短信验证码发送实现类（用户可以自己实现，bena名称：smsCodeSender）
 */
@Configuration
public class ValidateCodeBeanConfig {

	@Autowired
	private SecurityProperties securityProperties;
	
	/**
	 * 图形验证码生成实现
	 * 调用方应用可以自己实现一个验证码生成器翻入容器中，取名imageValidateCodeGenerator，则Spring
	 * 就会使用应用提供的生成器而不会创建这个默认的生成器
	 * @return
	 */
	@Bean("imageValidateCodeGenerator")
	@ConditionalOnMissingBean(name = "imageValidateCodeGenerator")
	public ImageCodeGenerator imageCodeGenerator() {
		ImageCodeGenerator imageCodeGenerator = new ImageCodeGenerator();
		imageCodeGenerator.setSecurityProperties(securityProperties);
		return imageCodeGenerator;
	}
	
	/**
	 * 短信验证码发送实现
	 * @return
	 */
	@Bean
	@ConditionalOnMissingBean(name = "smsCodeSender")
	public DefaultSmsCodeSender smsCodeSender() {
		DefaultSmsCodeSender smsCodeSender = new DefaultSmsCodeSender();
		return smsCodeSender;
	}
	
}
