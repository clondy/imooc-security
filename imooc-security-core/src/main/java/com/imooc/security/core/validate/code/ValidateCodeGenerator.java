package com.imooc.security.core.validate.code;

import javax.servlet.http.HttpServletRequest;

/**
 * 校验码生成器（图形、短信）
 * @author JZH
 *
 */
public interface ValidateCodeGenerator {

	ValidateCode generate(HttpServletRequest request);
	
}
