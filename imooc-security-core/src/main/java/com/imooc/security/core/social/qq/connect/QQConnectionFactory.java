package com.imooc.security.core.social.qq.connect;

import org.springframework.social.connect.support.OAuth2ConnectionFactory;

import com.imooc.security.core.social.qq.api.QQ;

public class QQConnectionFactory extends OAuth2ConnectionFactory<QQ> {

	
	public QQConnectionFactory(String providerId, String appId, String appSecret) {
		
		/**
		 * 
		 * @param providerId：提供商的唯一标识
		 * @param serviceProvider：
		 * @param apiAdapter
		 */
		super(providerId, new QQServiceProvider(appId, appSecret), new QQAdapter());
		// TODO Auto-generated constructor stub
	}

}
