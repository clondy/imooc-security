package com.imooc.security.core.validate.code;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.ServletWebRequest;

/**
 * 请求获取图形验证码
 * @author JZH
 *
 */
@RestController
public class ValidateCodeController {
	

	@Autowired
	private ValidateCodeProcessorHolder validateCodeProcessorHolder;

	@GetMapping("code/{type}")
	public void createValidateCode(HttpServletRequest request, HttpServletResponse response, @PathVariable("type") String type) throws Exception {
		ValidateCodeProcessor processor = validateCodeProcessorHolder.findValidateCodeProcessor(type);
		processor.createCode(new ServletWebRequest(request, response));
	}

	
}
