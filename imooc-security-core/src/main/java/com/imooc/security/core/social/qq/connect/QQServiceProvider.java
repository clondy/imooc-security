package com.imooc.security.core.social.qq.connect;

import org.springframework.social.oauth2.AbstractOAuth2ServiceProvider;
import org.springframework.social.oauth2.OAuth2Template;

import com.imooc.security.core.social.qq.api.QQ;
import com.imooc.security.core.social.qq.api.QQImpl;

/**
 * 负责整个OAuth2认证的类
 * @author JZH
 * 
 * ProviderService是负责整个OAuth2认证的，需要两个组件：
 * 1. OAuth2Operations：请求用户是否允许从服务提供商获取信息；向服务提供商请求获取token
 * 2. AbstractOAuth2ApiBinding：用上一步获取的token从服务提供商获取用户信息
 *
 */
public class QQServiceProvider extends AbstractOAuth2ServiceProvider<QQ> {

	private String appId;
	
	private static final String AUTHORIZE_URL = "https://graph.qq.com/oauth2.0/authorize";
	
	private static final String ACCESS_TO_TOKEN_URL = "https://graph.qq.com/oauth2.0/token";
	
	/**
	 * Spring默认的OAuth2Operations接口实现类
	 * @param oauth2Operations
	 */
	public QQServiceProvider(String appId, String appSecret) {
		/*
		 * clientId：appid
		 * clientSecret：app secret
		 * authorizeUrl：将用户导向服务提供商询问是否同意获取用户信息
		 * accessTokenUrl：第三方应用使用授权码向服务提供商获取token
		 */
		super(new OAuth2Template(appId, appSecret, AUTHORIZE_URL, ACCESS_TO_TOKEN_URL));
		// TODO Auto-generated constructor stub
	}

	/**
	 * 获取API组件
	 */
	@Override
	public QQ getApi(String accessToken) {
		// TODO Auto-generated method stub
		return new QQImpl(accessToken, appId);
	}

}
