package com.imooc.security.core.validate.code;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.SecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.DefaultSecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.stereotype.Component;

/**
 * 认证配置
 * @author JZH
 * 
 * 将校验码过滤器配置到Spring Security的过滤器链开始位置
 * 注意：
 * 使用@Component可以由用户决定是否需要校验验证码
 */
@Component("validateCodeAuthenticationConfig")
public class ValidateCodeAuthenticationConfig extends SecurityConfigurerAdapter<DefaultSecurityFilterChain, HttpSecurity> {

	@Autowired
	private ValidateCodeFilter validateCodeFilter;
	
	/**
	 * 将验证码校验过滤器配置到UsernamePasswordAuthenticationFilter前
	 */
	@Override
	public void configure(HttpSecurity http) throws Exception {
		http.addFilterBefore(validateCodeFilter, UsernamePasswordAuthenticationFilter.class);
	}
	
}
