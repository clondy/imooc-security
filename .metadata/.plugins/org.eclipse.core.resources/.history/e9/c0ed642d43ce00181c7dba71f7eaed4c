package com.imooc.security.core.validate.code.controller;

import java.io.IOException;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.connect.web.HttpSessionSessionStrategy;
import org.springframework.social.connect.web.SessionStrategy;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.ServletWebRequest;

import com.imooc.security.core.sms.SmsCodeSender;
import com.imooc.security.core.validate.code.ImageCode;
import com.imooc.security.core.validate.code.ValidateCode;
import com.imooc.security.core.validate.code.generator.ValidateCodeGenerator;

/**
 * 请求获取图形验证码
 * @author JZH
 *
 */
@RestController
public class ValidateCodeController {
	
	private static final String SESSION_KEY_IMAGECODE = "SESSION_KEY_IMAGECODE";
	
	private static final String SESSION_KEY_SMSCODE = "SESSION_KEY_SMSCODE";

	private SessionStrategy sessionStrategy = new HttpSessionSessionStrategy();
	
	@Autowired
	private ValidateCodeGenerator imageCodeGenerator;
	
	@Autowired
	private ValidateCodeGenerator smsCodeGenerator;
	
	@Autowired
	private SmsCodeSender smsCodeSender;

	
	/**
	 * 1. 调用生成验证码方法
	 * 2. 存储到session中
	 * 3. 返回验证码输出流
	 * @param request
	 * @param repsonse
	 * @throws IOException
	 */
	@GetMapping("code/image")
	public void createImageCode(HttpServletRequest request, HttpServletResponse repsonse) throws IOException {
		ImageCode imageCode = (ImageCode) imageCodeGenerator.generate(request);
		sessionStrategy.setAttribute(new ServletWebRequest(request), SESSION_KEY_IMAGECODE, imageCode);
		ImageIO.write(imageCode.getImage(), "JPEG", repsonse.getOutputStream());
	}
	
	/**
	 * 1. 调用短信验证码生成器
	 * 2. 存储到session中
	 * 3. 调用短信发送接口发送验证码
	 * @param request
	 * @param repsonse
	 * @throws IOException
	 * @throws ServletRequestBindingException
	 */
	@GetMapping("code/sms")
	public void createSmsCode(HttpServletRequest request, HttpServletResponse repsonse) throws IOException, ServletRequestBindingException {
		ValidateCode smsCode = smsCodeGenerator.generate(request);
		sessionStrategy.setAttribute(new ServletWebRequest(request), SESSION_KEY_SMSCODE, smsCode);
		String mobile = ServletRequestUtils.getRequiredStringParameter(request, "mobile");
		smsCodeSender.send(mobile, smsCode.getCode());
	}
	
	public static String getSessionImageCodeKey() {
		return SESSION_KEY_IMAGECODE;
	}
	
	public static String getSessionSmsCodeKey() {
		return SESSION_KEY_SMSCODE;
	}
	
}
