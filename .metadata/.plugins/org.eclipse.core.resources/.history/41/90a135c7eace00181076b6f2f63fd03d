package com.imooc.security.browser;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.rememberme.JdbcTokenRepositoryImpl;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;

import com.imooc.security.browser.authentication.ImoocAuthenticationFailureHandler;
import com.imooc.security.browser.authentication.ImoocAuthenticationSuccessHandler;
import com.imooc.security.core.authentication.mobile.SmsCodeAuthenticationSecurityConfig;
import com.imooc.security.core.properties.SecurityProperties;
import com.imooc.security.core.validate.code.ValidateCodeAuthenticationConfig;
import com.imooc.security.core.validate.code.ValidateCodeProcessorHolder;

/**
 * 
 * @author JZH
 *
 */
@Configuration
public class BrowserSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private SecurityProperties securityProperties;

	@Autowired
	private ImoocAuthenticationSuccessHandler imoocAuthenticationSuccessHandler;
	
	@Autowired
	private ImoocAuthenticationFailureHandler imoocAuthenticationFailureHandler;
	
	@Autowired
	private UserDetailsService userDetailsService;
	
	@Autowired
	private ValidateCodeProcessorHolder validateCodeProcessorHolder;
	
	@Autowired
	private ValidateCodeAuthenticationConfig validateCodeAuthenticationConfig;
	
	/**
	 * 注入 自定义的一套认证组件配置，注入后，自定义组件生效
	 */
	@Autowired
	private SmsCodeAuthenticationSecurityConfig smsCodeAuthenticationSecurityConfig;
	
	/**
	 * 会使用具体应用的数据源
	 */
	@Autowired
	private DataSource dataSource;
	
	/**
	 * 对用户的token操作（从数据库中存储、取出），实现记住我的功能
	 * @return
	 */
	@Bean
	public PersistentTokenRepository persistentTokenRepository() {
		JdbcTokenRepositoryImpl tokenRepository = new JdbcTokenRepositoryImpl();
		tokenRepository.setDataSource(dataSource);
		//tokenRepository.setCreateTableOnStartup(true);//首次启动的时候创建表
		return tokenRepository;
	}

	/**
	 * 密码加密解密
	 * 
	 * @return
	 */
	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		// TODO Auto-generated method stub
		
//		ValidateCodeFilter validateCodeFilter = new ValidateCodeFilter();
//		validateCodeFilter.setAuthenticationFailureHandler(imoocAuthenticationFailureHandler);
//		validateCodeFilter.setSecurityProperties(securityProperties);
//		validateCodeFilter.setValidateCodeProcessorHolder(validateCodeProcessorHolder);
//		validateCodeFilter.afterPropertiesSet();
		
		
		//配置
		http
		//.addFilterBefore(validateCodeFilter, UsernamePasswordAuthenticationFilter.class) //在Security过滤器链的最开始新增验图形证码过滤器
		//.addFilterBefore(smsFilter, UsernamePasswordAuthenticationFilter.class) //在Security过滤器链的最开始新增短信验证码过滤器
		.formLogin()
			.loginPage("/authentication/require") // 需要身份认证的时候跳转的URL（可以直接指定html）
			.loginProcessingUrl("/authentication/form") // 指定登陆请求url
			.successHandler(imoocAuthenticationSuccessHandler) // 指定登陆成功的处理方式是自定义实现
			.failureHandler(imoocAuthenticationFailureHandler) //指定登陆失败的处理方式是自定义实现
		.and()
			.rememberMe()
			.tokenRepository(persistentTokenRepository())
			.tokenValiditySeconds(securityProperties.getBrowser().getRememberMeSeconds())
			.userDetailsService(userDetailsService) //获取token并从数据库中取出用户名后需要进行登陆
			// http.httpBasic()
			.and().authorizeRequests() // 授权的配置
			.antMatchers("/authentication/require", securityProperties.getBrowser().getLoginPage(), "/code/*").permitAll() // 跳转URL、访问登陆界面不需要授权认证
			.anyRequest() // 任何请求
			.authenticated().and().csrf().disable() // 需要身份认证
			.apply(smsCodeAuthenticationSecurityConfig) //引入其他配置
			.and().apply(validateCodeAuthenticationConfig); 
	}

}
