package com.imooc.security.core.validate.code.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.social.connect.web.HttpSessionSessionStrategy;
import org.springframework.social.connect.web.SessionStrategy;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.filter.OncePerRequestFilter;

import com.imooc.security.core.validate.code.ImageCode;
import com.imooc.security.core.validate.code.ValidateCodeController;
import com.imooc.security.core.validate.code.exception.ValidateCodeException;

/**
 * 登录请求的时候校验图形验证码
 * @author JZH
 * 
 * OncePerRequestFilter：Spring的一个工具类，保证过滤器每次只会被执行一次
 */
public class ValidatorCodeFilter extends OncePerRequestFilter {
	
	@Autowired
	private AuthenticationFailureHandler authenticationFailureHandler;
	
	private SessionStrategy sessionStrategy = new HttpSessionSessionStrategy();

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		if(StringUtils.equals("/authentication/form", request.getRequestURI()) && StringUtils.equalsIgnoreCase(request.getMethod(), "post")) {
			try {
				validate(new ServletWebRequest(request));
			} catch(ValidateCodeException e) {
				authenticationFailureHandler.onAuthenticationFailure(request, response, e);
			}
		} else {
			filterChain.doFilter(request, response);
		}
	}
	
	private void validate(ServletWebRequest request) throws ServletRequestBindingException {
		ImageCode imageCode = (ImageCode) sessionStrategy.getAttribute(request, ValidateCodeController.getSessionImageCodeKey());
		String codeInRequest = ServletRequestUtils.getStringParameter(request.getRequest(), "imageCode");
		if(StringUtils.isBlank(codeInRequest)) {
			throw new ValidateCodeException("验证码不能为空");
		}
		if(null == imageCode) {
			throw new ValidateCodeException("验证码不存在");
		}
		if(imageCode.is) {
			throw new ValidateCodeException("验证码已过期");
		}
		if(!StringUtils.equals(imageCode.getCode(), codeInRequest)) {
			throw new ValidateCodeException("验证码不匹配");
		}
	}

}
