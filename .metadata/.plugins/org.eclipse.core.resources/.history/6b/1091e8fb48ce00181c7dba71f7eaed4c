package com.imooc.security.core.validate.code;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.connect.web.HttpSessionSessionStrategy;
import org.springframework.social.connect.web.SessionStrategy;
import org.springframework.web.context.request.ServletWebRequest;

import com.imooc.security.core.validate.code.exception.ValidateCodeException;

/**
 * 抽象的模板方法
 * @author JZH
 *
 */
public abstract class AbstractValidateCodeProcessor<T extends ValidateCode> implements ValidateCodeProcessor {

	/**
	 * session工具类，用于将验证码存储到session中
	 */
	private SessionStrategy sessionStrategy = new HttpSessionSessionStrategy();
	
	@Autowired
	private Map<String, ValidateCodeGenerator> validateCodeGenerators;
	
	@Override
	public final void createCode(ServletWebRequest request)  throws Exception {
		// TODO Auto-generated method stub
		T validateCode = generate(request);
		save(request, validateCode);
		send(request, validateCode);
	}
	
	/**
	 * 生成校验码
	 * 使用依赖查找的方式选择ValidateCodeGenerator的实现类
	 * @param request
	 * @return
	 */
	public T generate(ServletWebRequest request) {
		String type = StringUtils.substringBefore(getClass().getSimpleName(), "CodeProcessor").toLowerCase();
		String generatorName = type+ValidateCodeGenerator.class.getSimpleName();
		ValidateCodeGenerator generator = validateCodeGenerators.get(generatorName);
		if(null == generator) {
			throw new ValidateCodeException("验证码生成器"+generatorName+"不存在");
		}
		@SuppressWarnings("unchecked")
		T t = (T) generator.generate(request.getRequest());
		return t;
	}
	
	/**
	 * 将校验码放入session中
	 * @param request
	 * @param t 校验码
	 */
	public void save(ServletWebRequest request, T t) {
		String key = getValidateCodeSessionKey();
		sessionStrategy.setAttribute(request, key, t);
	}
	
	/**
	 * 发送校验码给客户端
	 * @param request
	 * @param t
	 */
	public abstract void send(ServletWebRequest request, T t) throws Exception;
	
	private String getValidateCodeSessionKey() {
		String type = StringUtils.substringBefore(getClass().getSimpleName(), "CodeProcessor");
		String key = SESSION_KEY_PREFIX +type.toUpperCase();
		return key;
	}

}
