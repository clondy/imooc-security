package com.imooc.security.core.validate.code.filter;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.social.connect.web.HttpSessionSessionStrategy;
import org.springframework.social.connect.web.SessionStrategy;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.filter.OncePerRequestFilter;

import com.imooc.security.core.properties.SecurityProperties;
import com.imooc.security.core.validate.code.ValidateCode;
import com.imooc.security.core.validate.code.ValidateCodeController;
import com.imooc.security.core.validate.code.ValidateCodeProcessor;
import com.imooc.security.core.validate.code.exception.ValidateCodeException;

/**
 * 登录请求的时候校验短信验证码
 * @author JZH
 * 
 * OncePerRequestFilter：Spring的一个工具类，保证过滤器每次只会被执行一次
 */
public class SmsCodeFilter extends OncePerRequestFilter implements InitializingBean {
	
	@Autowired
	private AuthenticationFailureHandler authenticationFailureHandler;
	
	private SessionStrategy sessionStrategy = new HttpSessionSessionStrategy();
	
	private Set<String> urls = new HashSet<>();
	
	@Autowired
	private SecurityProperties securityProperties;
	
	private AntPathMatcher antPathMatcher = new AntPathMatcher(); //spring的工具类
	
	/**
	 * InitializingBean接口规范的初始化方法
	 * 处理需要校验图形验证码的url集合
	 */
	@Override
	public void afterPropertiesSet() throws ServletException {
		// TODO Auto-generated method stub
		super.afterPropertiesSet();
		String[] urlArray = StringUtils.splitByWholeSeparatorPreserveAllTokens(securityProperties.getCode().getSms().getUrls(), ",");
		for(String url : urlArray) {
			urls.add(url);
		}
		urls.add("/authentication/mobile"); //登陆请求必须由验证码校验
	}

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		boolean needCodeValidate = false;
		for(String url : urls) {
			if(antPathMatcher.match(url, request.getRequestURI())) {
				needCodeValidate = true;
				break;
			}
		}
		if(needCodeValidate) {
			try {
				validate(new ServletWebRequest(request));
			} catch(ValidateCodeException e) {
				authenticationFailureHandler.onAuthenticationFailure(request, response, e);
				return; //认证失败不能执行后续过滤器
			}
		}
		filterChain.doFilter(request, response);
	}
	
	private void validate(ServletWebRequest request) throws ServletRequestBindingException {
		String sessionKey = ValidateCodeProcessor.SESSION_KEY_PREFIX + "SMS";
		ValidateCode smsCode = (ValidateCode) sessionStrategy.getAttribute(request, sessionKey);
		String codeInRequest = ServletRequestUtils.getStringParameter(request.getRequest(), "smsCode");
		if(StringUtils.isBlank(codeInRequest)) {
			throw new ValidateCodeException("验证码不能为空");
		}
		if(null == smsCode) {
			throw new ValidateCodeException("验证码不存在");
		}
		if(smsCode.isExpired()) {
			sessionStrategy.removeAttribute(request, sessionKey);
			throw new ValidateCodeException("验证码已过期");
		}
		if(!StringUtils.equals(smsCode.getCode(), codeInRequest)) {
			throw new ValidateCodeException("验证码不匹配");
		}
		sessionStrategy.removeAttribute(request, sessionKey);
	}

	public AuthenticationFailureHandler getAuthenticationFailureHandler() {
		return authenticationFailureHandler;
	}

	public void setAuthenticationFailureHandler(AuthenticationFailureHandler authenticationFailureHandler) {
		this.authenticationFailureHandler = authenticationFailureHandler;
	}

	public SecurityProperties getSecurityProperties() {
		return securityProperties;
	}

	public void setSecurityProperties(SecurityProperties securityProperties) {
		this.securityProperties = securityProperties;
	}
	
	

}
